//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(window).on('load', function () {
    setTimeout(function () {
        $('.loader').fadeOut();
    }, 1500);
    $('.error-message').hide();
})


$(".next").click(function () {


    current_fs = $(this).parent();

    var isanyfieldempty = false;
    current_fs.find(':input').each(function () {
        if ($('input[name=' + this.name + ']').val() == "") {
            isanyfieldempty = true;

        }
    });
    if (!isanyfieldempty) {
        $('.error-message').hide();
        if (animating) return false;
        animating = true;
        next_fs = $(this).parent().next();
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();


        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'position': 'absolute'
                });
                next_fs.css({
                    'left': left,
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
                /*current_fs.css(
                    {
                        'position': 'relative'
                    }
                );*/
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });


    } else {
        $('.error-message').show();
        //SHOW ERROR MESSAGE THAT ALL FIELD IS COMPULSORY
        //console.log(isanyfieldempty);
    }


});

$(".previous").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
        opacity: 0
    }, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'left': left
            });
            previous_fs.css({
                'transform': 'scale(' + scale + ')',
                'opacity': opacity
            });
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
            previous_fs.css({
                'position': 'relative'
            });
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

/*OPEN WHY US*/
$('.why-us-btn').click(function () {
    $('.why-us-wrapper').fadeIn();
    $('.why-us-content').removeClass('animated fadeOut');
    $('.why-us-content').addClass('animated fadeInUp');
    $('.why-us-content').show();
});
$('.why-us-wrapper').click(function () {
    $('.why-us-wrapper').fadeOut();
    $('.why-us-content').removeClass('animated fadeInUp');
    $('.why-us-content').addClass('animated fadeOut');
    setTimeout(function () {
        $('.why-us-content').hide();
    }, 800);
})

$("#submitForm").click(function (event) {
    event.preventDefault();
    var name = $('input[name=name]').val();
    var dob = $('input[name=dob]').val();
    var gender = $('input[name=gender]:checked').val();
    var school = $('input[name=school]').val();
    var email = $('input[name=email]').val();
    var contact = $('input[name=contact]').val();
    var center = $('select[name=center]').val();

    console.log(name, dob, gender, school, email, contact, center);
    var userdetail = {
        'name': name,
        'dateofbirth': dob,
        'gender': gender,
        'school': school,
        'email': email,
        'contact': contact,
        'center': center
    };

    $('#msform').addClass('animated bounceOut');
    $('.why-us-btn').addClass('animated bounceOut');
    setTimeout(function () {
        $('.success-class').addClass('animated bounceIn');
        $('.success-class').show();
        $('.before-success').show();
        $('.after-success').hide();
    }, 1000);


    $.post("http://www.pixoloproductions.com/fcb/fcbescolarest/index.php/fcbregistrations/postinsert", {
            data: JSON.stringify(userdetail)
        },
        function (result) {
            console.log(result);
            if (result != "") {
                setTimeout(function () {
                    console.log("show success block");
                    $('.before-success').hide();
                    $('.after-success').addClass('animated bounceIn');
                    $('.after-success').show();
                }, 3000);
                $.get("http://www.pixoloproductions.com/fcb/fcbescolarest/fcbregistrationmail.php", userdetail);


            }

        });





    return false;


})


$('.register-btn').click(function () {
    location.reload();

});
